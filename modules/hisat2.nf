process hisat2 {

    label 'highmem'

    tag "hisat2"

    publishDir "${params.resultdir}/04a_hisat2",	mode: 'copy', pattern: '*.ht2'
    publishDir "${params.resultdir}/04a_hisat2",	mode: 'copy', pattern: '*.sam'
    publishDir "${params.resultdir}/logs/hisat2",	mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: '*.cmd'

    input:
        path(illumina_r1_ch)
        path(illumina_r2_ch)
        path(red_ch)

    output:
        path("*.ht2")
        path("*.sam"), emit: sam_ch
        path("*.log"), emit: log_ch
        path("*.cmd")
        
    script:
    """
    hisat2.sh $illumina_r1_ch $illumina_r2_ch $red_ch ${task.cpus} hisat2.cmd >& hisat2.log 2>&1
    """ 
}
