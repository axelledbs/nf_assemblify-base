process redmask {

    label 'midmem'

    tag "redmask"

    publishDir "${params.resultdir}/03_redmask",	mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/redmask",	mode: 'copy', pattern: 'redmask*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'redmask*.cmd'

    input:
        path(assembly_fa)

    output:
        path("masked/*.msk"), emit: red_ch
        path("redmask*.log")
        path("redmask*.cmd")

    script:
    """
    redmask.sh $assembly_fa ${task.cpus} redmask.cmd >& redmask.log 2>&1
    """ 
}




