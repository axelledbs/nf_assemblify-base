# Assemblify-base: FAIR Genome Assembly and Annotation Pipeline

## Introduction

Assemblify-base is a genome assembly pipeline designed to incorporate the FAIR principles using [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) (version: 23.04.1)(Di Tommaso et al., 2017). This pipeline accommodates various types of sequences, including transcriptomic (RNA) and genomic (DNA) sequences, sourced from either Illumina or PacBio-HiFi sequencers.

The pipeline encompasses five main stages : quality control of reads, assembly into contigs, masking of repeated regions, and evidences and prediction. The utilization of Nextflow empowers users to execute analyses seamlessly via a straightforward command line. Each analysis generates results folders, named according to the respective pipeline stage (refer to the *Pipeline sketch* section). The automated nature of the pipeline ensures high reproducibility of analyses.

Unfortunatly, the pipeline is still under developement, hence, the prediction step is yet to be included in the pipeline.

## Biological Datasets

For this analysis, we utilized the genome of *Dunaliella primolecta*, a micro-algae species. Two types of data were used : RNA sequences from an Illumina sequencer and DNA sequences from a PacBio HiFi sequencer.

- Study Accession Number: PRJEB46283
  - [ENA (EMBL-EBI)](https://www.ebi.ac.uk/ena/browser/view/PRJEB46283)
  - [BioProject (NCBI)](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB46283/)

- Samples Accession Numbers:
  - RNA PolyA: SAMEA7524530
    - [BioSamples (EMBL-EBI)](https://www.ebi.ac.uk/biosamples/samples/SAMEA7524530)
    - [BioSample (NCBI)](https://www.ncbi.nlm.nih.gov/biosample/SAMEA7524530)
  - PacBio - HiFi: SAMEA8100059
    - [BioSamples (EMBL-EBI)](https://www.ebi.ac.uk/biosamples/samples/SAMEA8100059)
    - [BioSample (NCBI)](https://www.ncbi.nlm.nih.gov/biosample/?term=SAMEA8100059)

- Run Accession Numbers:
  - RNA PolyA: ERR6688549
    - [ENA (EMBL-EBI)](https://www.ebi.ac.uk/ena/browser/view/ERR6688549)
    - [SRA (NCBI)](https://trace.ncbi.nlm.nih.gov/Traces/?view=run_browser&acc=ERR6688549&display=metadata)
  - PacBio - HiFi: ERR6939244
    - [ENA (EMBL-EBI)](https://www.ebi.ac.uk/ena/browser/view/ERR6939244)
    - [SRA (NCBI)](https://trace.ncbi.nlm.nih.gov/Traces/?view=run_browser&acc=ERR6939244&display=metadata)


## Pipeline sketch

![Pipeline Sketch](https://practical-case-bioinfo-teaching-fair-gaa-92d332f8346e321adf6fde.gitlab-pages.ifremer.fr/_imgs/pipeline-gaa.png)

```
./
`-- assemblify
    |-- 00_pipeline_info
    |   `-- cmd
    |-- 01_reports
    `-- 02_results
        |-- 01a_fastqc
        |-- 01b_multiqc
        |-- 01c_nanoplot
        |-- 02a_hifiasm
        |-- 02c_busco
        |-- 03_redmask
        |-- 04a_hisat2
        `-- logs
```


## Computing Environment

This pipeline was developed and implemented on the [On-Demand cluster](https://ondemand.cluster.france-bioinformatique.fr) with the following computing environment:

- OS: Unix
- Partition: fast
- Number of CPUs: 2
- Amount of Memory: 8G

## Requirements

Before starting the pipeline, users must:

- Install [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) and [Singularity](https://docs.sylabs.io/guides/3.0/user-guide/).
- Clone the GitLab fork of *Nf_Assemblify-base* (IFB):
  - Click on **Fork project** on GitLab web interface.
  - Copy your project link and create your git repository with the following command lines:
    ```bash
    mkdir assemblify-tmp
    git clone <your_gitlab_fork_of_assembly-base_project>*
    ```
- Install the tools with the following version :
    - [Fastqc](https://github.com/s-andrews/FastQC) (>= 0.11.9) 
    - [Multiqc](https://github.com/MultiQC/MultiQC) (>= 1.14)
    - [hifiasm](https://github.com/chhylp123/hifiasm) (>= 0.18.9)
    - [nanoplot](https://github.com/wdecoster/NanoPlot) (>= 1.28.4)
    - [busco](https://github.com/metashot/busco) (>= 5.5.0)
    - [redmask](https://github.com/nextgenusfs/redmask) (>= 2018.09.10)
    - [hisat2](https://github.com/DaehwanKimLab/hisat2) (>= 2.2.1)

# Licence

Assemblify-base pipeline © 2024 by Axelle Dubois & Diane Roy is licensed under [Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc/4.0/).

[![CC BY-NC 4.0 License](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc.png)]


## Authors
* Axelle Dubois, M2 bioinformatics student, University of Rennes, France
* Diane Roy, M2 bioinformatic student, University of Rennes, France  

## Contributions and Feedback

We welcome contributions and feedback from the community. If you encounter any issues, have suggestions for improvements, or want to contribute to the development, please contact us at our email adress :
axelle.dubois@etudiant.univ-rennes.fr diane.roy@etudiant.univ-rennes.fr



