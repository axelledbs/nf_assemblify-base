#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Mask repeats from genome (denovo)                        ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
GENOME=${args[0]}
NCPUS=${args[1]}
LOGCMD=${args[2]}

CMD="Red -gnm . -msk masked -cor $NCPUS"

# Keep command in log
echo "mkdir -p masked"
echo ${CMD} > ${LOGCMD}

#Create a subdirectory to store results from Red 
mkdir -p masked

# Run Command
eval ${CMD}


