#!/usr/bin/env bash
############################################################################################################
##                                                                                                        ##
##        Hisat2 Graph-based alignment of next generation sequencing reads to a population of genomes     ##
##                                                                                                        ##
############################################################################################################

# var settings
args=("$@")
R1=${args[0]}
R2=${args[1]}
MSK_FASTA=${args[2]} #Output from RED
NCPUS=${args[3]}
LOGCMD=${args[4]}

# Constant
INTRON=20000

SAM_OUT=$(basename ${R1%.fastq*}).sam
IDX=${MSK_FASTA%.*}

# Command to execute
CMD="hisat2-build -p $NCPUS $MSK_FASTA ${MSK_FASTA%.*} ; \
	hisat2 -p $NCPUS --no-unal -q -x $IDX -1 ${R1} -2 ${R2} --max-intronlen $INTRON > $SAM_OUT"
       
# Keep command in log
echo $CMD > $LOGCMD

# Run Commands
eval $CMD

