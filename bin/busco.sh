#!/usr/bin/env bash
############################################################################################################
##                                                                                                        ##
##              Quantitative assessment of genome assembly with Busco                                     ##
##                                                                                                        ##
############################################################################################################

# Get script arguments coming from modules/fastqc.nf process
args=("$@")
NCPUS=${args[0]}
BUSCO_DB_PATH=${args[1]}
BUSCO_DB_NAME=${args[2]}
ASSEMBLY_FILE=${args[3]}
LOGCMD=${args[4]}

# Command to execute
RES_FILE=$(basename ${ASSEMBLY_FILE%.*}).busco
CMD="busco -c ${NCPUS} --offline -m genome -i ${ASSEMBLY_FILE} -l ${BUSCO_DB_PATH}/${BUSCO_DB_NAME} -o ${RES_FILE}"

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}


