#!/usr/bin/env bash
############################################################################################################
##                                                                                                        ##
##              Plotting for long read sequencing data and alignments with Nanoplot                       ##
##                                                                                                        ##
############################################################################################################

# Get script arguments coming from modules/fastqc.nf process
args=("$@")
FASTQ_FILE=${args[0]}
LOGCMD=${args[2]}
cpus=${args[1]}

# Command to execute
CMD="NanoPlot -t ${cpus} --plots kde dot hex --N50 \
         --title \"PacBio Hifi reads for $(basename ${FASTQ_FILE%.hifi*})\" \
         --fastq ${FASTQ_FILE} -o ."

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}